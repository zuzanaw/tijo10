package pl.edu.pwsztar.mapper;

@FunctionalInterface
public interface Converter<T, F>{
    T convert(F from);
}
