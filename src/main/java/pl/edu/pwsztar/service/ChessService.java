package pl.edu.pwsztar.service;

import pl.edu.pwsztar.domain.dto.FigureMoveDto;
import pl.edu.pwsztar.domain.enums.Point2D;

public interface ChessService {
    Point2D toPoint(FigureMoveDto figureMoveDto);
}
